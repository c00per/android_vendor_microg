PRODUCT_SOONG_NAMESPACES += \
    vendor/microg

WITH_MICROG := true

PRODUCT_COPY_FILES += \
	$(call find-copy-subdir-files,*,vendor/microg/product_copy_files,$(TARGET_COPY_OUT_SYSTEM)/)

# framework
PRODUCT_PACKAGES += \
	com.google.android.maps \
	com.google.android.media.effects \
	com.google.widevine.software.drm

# core microg
PRODUCT_PACKAGES += \
	FakeStore \
	GoogleBackupTransport \
	GoogleCalendarSyncAdapter \
	GoogleContactsSyncAdapter \
	MicroGGMSCore \
	MicroGGSFProxy

ifeq ($(WITH_AURORA),true)
PRODUCT_PACKAGES += \
	AuroraServices \
	AuroraDroid \
	AuroraStore
else
PRODUCT_PACKAGES += F-Droid
endif

ifeq ($(WITH_BACKENDS),true)
PRODUCT_PACKAGES += \
	AppleNLPBackend \
	DejaVuNLPBackend \
	LocalGSMNLPBackend \
	LocalWiFiNLPBackend \
	MozillaUnifiedNLPBackend \
	NominatimNLPBackend
endif
